\section{Problem definition}
\label{s:problem}

This section characterizes the problem of identifying and generalizing repetitive code edits.
We start by describing an example of repetitive change applied to Roslyn, Microsoft's library for compilation and code analysis for C\# and
VB.NET~\cite{roslyn}.
%describe the change
In the diff in Figure~\ref{fig:example1}, red lines (``\texttt{-}'' sign) show the original code while green lines (``\texttt{+}'' sign) show the code after the change.
In the original code, developers use \code{==} operator to compare the value returned by the method \code{CSharpKind} with a specific syntax kind.
Developers replaced these comparison operators by method invocations of the new method \code{IsKind}, passing the right-hand side expression as argument.
Commit \textit{8c14644}\footnote{\href{https://github.com/dotnet/roslyn/commit/8c14644}{https://github.com/dotnet/roslyn/commit/8c14644}} shows that developers applied this repetitive change to 26 code fragments in the source code.

%problem
However, by using our technique, we determined that developers had missed 722 code fragments where they could apply this repetitive change.
The lack of tool support, effort to manually apply it, and unawareness of all code fragments to change may contribute to this high number of missed code fragments.
%interacting with developers
We opened an issue in Roslyn's repository to apply this change to make the code consistent.
Developers confirmed the change should be applied, and later we submitted a pull request that is merged with Roslyn source code.
 \begin{figure}
 	\centering
 	\includegraphics[scale=0.5]{images/example1.png}
 	\caption{Repetitive change applied to Roslyn's source code to replace an equals expression to a method invocation.}
 	\label{fig:example1}
 \end{figure}

We found the aforementioned change during our preliminary investigation to understand the nature of repetitive changes.
To do so, we manually inspected \totalNumberOfCommits{} commits from three open source projects: Roslyn, Entity Framework~\cite{entity}, and NuGet~\cite{nuget}.
In this work, we consider a change to be repetitive if it contains more than two code fragments.
In addition, we interact with developers to perform external validation and collect insights to better implement our technique.

To guide our investigation, we raise the following questions:

\begin{itemize}
\item \textbf{\researchQuestion{}1: How frequent are repetitive changes?} Our work relies on the existence of repetitive changes.
We are interested in whether this problem is frequent;
\item \textbf{\researchQuestion{}2:  What is the distribution of code fragments per repetitive changes?} We investigate the number of code fragments that a developer has to change to complete the task;
\item \textbf{\researchQuestion{}3: How sparse are repetitive changes?} We measure how spread the code fragments are among different files;
\item \textbf{\researchQuestion{}4: Are repetitive changes similar or identical?} Some changes require the same modifications within the code fragments (identical) while others require similar modifications.
We investigated such property because if the code fragments of a repetitive change are identical, developers can simply copy and paste the modification.
\end{itemize}

To answer~\researchQuestion{1}, we identified occurrences of repetitive changes.
Analyzing the collected data, we identified~\numberOfRepetitiveChanges{} of distinct repetitive change scenarios:~\numberOfChangesRoslyn{},~\numberOfChangesEntityFramework{}, and~\numberOfChangesNuGet{} changes for Roslyn, Entity Framework and NuGet, respectively.
Besides the meaningful number of repetitive changes, the number of code fragments on those changes is large enough to motivate our work.
Answering~\researchQuestion{}2, the number of code fragments per change range from 3 up to 60 with median of~\medianPerChange{}, and each project contains changes with at least 19 code fragments.
Regarding~\researchQuestion{}3, we identify that \numberOfMultipleFiles{} (\percentOfMultipleFiles{}\%) out of \numberOfRepetitiveChanges{} repetitive changes have code fragments located in more than one file.
This means that a meaningful number of the inspected changes require the analysis of different files.
In addition, developers might miss some of the code fragments. Therefore, this observation supports the need for a technique to help developers perform repetitive changes.
Finally, answering \researchQuestion{}4, one might ask whether copy and paste actions would  solve the problem of performing repetitive changes.
In fact, that would be the case if the code fragments are identical.
However,~\numberOfNonIdentical{} (\percentOfNonIdentical{}\%) out of the~\numberOfRepetitiveChanges{} repetitive changes do not contain identical code fragments.
Hence, simply copying and pasting changes would not complete the task.
We will discuss these changes and will show their importance to drive our implementation.

