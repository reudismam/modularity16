\section{Background}
\label{s:background}

We present some background on FlashExtract~\cite{LE14FLAS} and FlashFill~\cite{GU11AUTO} before discussing our approach that extends and combines these systems to perform repetitive change by examples.
Despite targeting entirely different problems, FlashExtract and FlashFill share some commonalities in their synthesis algorithms.
Given the examples, they both construct the version-space algebra that contains all possible programs satisfying the examples.
A ranking system then ranks these programs and returns the most likely one to the user.
What distinguishes them is their underlying domain-specific languages (DSLs), which we discuss next.

\subsection{FlashExtract}
\label{s:fe}

FlashExtract is an IP framework for extracting data from semi-structured documents such as text files, web pages, and spreadsheets.
Figure~\ref{fig:flashextract-example} illustrates the task of extracting all customers' first names from a file in FlashExtract.
The user only needs to highlight two examples of first names; FlashExtract learns an extraction program and highlights the rests automatically.


\begin{figure}[ht]
	\centering
	\includegraphics[scale=0.45]{images/flashextract-example.png}
	\caption{Extracting all first names from a text file in FlashExtract. User gives two examples in (a); FlashExtract learns a program from the examples, executes the program, and highlights all remaining names in (b).}
	\label{fig:flashextract-example}
\end{figure}

FlashExtract operates on \emph{region}, which is a two-dimensional block of the document defined by two character positions in a file.
FlashExtract' DSL encodes logics to extract relevant regions using regular expressions.
The DSL has four main operators: \textit{filter}, \textit{map}, \textit{pair}, and \textit{merge}.

\textbf{\textit{Filter Operator}}
The \textit{filter} operator has two parameters $\lambda x : B$ and $S$, where $B$ is a Boolean expression and $S$ is a sequence expression.
The filter operator selects elements in $S$ that satisfies $B$.
For example, the following filter program selects all lines that contain the names in Figure~\ref{fig:flashextract-example}:

\begin{mdframed}
	\begin{program}
		\label{p:filter-example}
		$nameLines$ = \textbf{FilterBool}($\lambda$x : $pred$, $lines$), where \newline
			\hspace*{2em} $pred$ = \texttt{StartsWith}(x, ``WordSpace'') \newline
			\hspace*{2em} $lines$ = \texttt{split}($R_{0}$, `\textbackslash{}n')
	\end{program}
 \end{mdframed}

In this program, the \texttt{split} function breaks the input file ($R_0$) into a list of lines.
This filter operator applies the predicate \texttt{StartsWith} on every line in the file, selecting only those that start with a word followed by a space.
This predicate is sufficient to distinguish the lines containing names from the other lines.


\textbf{\textit{Map Operator}}
The map operator has two parameters $\lambda x : F$ and $S$, where $S$ is a sequence expression of type \texttt{List<T>} and $F$ is a function that maps an element of type \texttt{T} to and element of type \texttt{T'}.
The map operator applies $F$ to every element of $S$ and returns a sequence of type \texttt{List<T'>}.

\textbf{\textit{Pair Operator}}
The pair operator is used to construct a region from its two constituent positions.

The following program uses map and pair to find the list of first names from the list of lines containing names (i.e., \emph{nameLines} from the previous example):

\begin{mdframed}
	\begin{program}\label{p:map}
	$program$ = \textbf{Map}($\lambda$x :  \textbf{Pair}($p_1$, $p_2$), $nameLines$), where \newline
		\hspace*{2em} $p_1$ = \texttt{AbsPos}(x, 0),
$p_2$ = \texttt{RegPos}(x, ``Word", ``Space", 1)
	\end{program}
\end{mdframed}

The program maps each line $x$ (that contains a name) to a first name region formed by $p_1$ and $p_2$.
The starting position $p_1$ is the beginning of the line (it is an absolute position).
The ending position $p_2$ is determined by the \textit{RegPos(Regex lr, Regex rr, int k)} operator, which returns the $k^{th}$ position that matches $lr$ on the left side, and matches $rr$ on the right side.
In this program, $p_2$ is the first position that matches a word on the left side, and matches a space on the right side.

\textbf{\textit{Merge Operator}} The \textit{merge} takes as input $n$ sequence expressions and combines their results.
This is useful when a single expression cannot extract all desired regions.




\subsection{FlashFill}
\label{s:ff}

FlashFill~\cite{GU11AUTO} is an IP technique for performing string transformations using input-output examples.
Figure~\ref{fig:flashfill-example} illustrate a task that normalizes names in an input column using examples.
The user only needs to give some examples in the output column; FlashFill induces a transformation program to fill the other cells of the output column automatically.

\begin{figure}[t]
	\centering
	\includegraphics[scale=0.5]{images/flashfill-example-2.png}
	\caption{Normalizing names by examples using FlashFill.}
	\label{fig:flashfill-example}
\end{figure}

Although FlashFill may take multiple input columns, for simplicity let us assume that it only takes one input column.
The output string is simply a concatenation of several strings, each of which is either derived from the input or a constant string. The logic to derive a substring from the input string is similar to the logic to extract a region in FlashExtract.

FlashFill learns the following program for the task described in Figure~\ref{fig:flashfill-example}:

	\begin{mdframed}[nobreak = true]
		 \begin{program}
program = \textbf{Concatenate}($s_1$, $s_2$, $s_3$, $s_4$)\newline
	\hspace{1em} $s_1$ = \textbf{Pair}($p_1$, $p_2$),
$p_1$ = \texttt{RegPos}(v, ``Space", $\epsilon$, 1),
$p_2$ = \texttt{AbsPos}(v, -1)\newline
	\hspace{1em} $s_2$ = \textbf{ConstStr}(``, ") \newline
 $s_3$ = \textbf{Pair}(\texttt{AbsPos}(v,0), \texttt{AbsPos}(v, 1)) \newline
$s_4$ = \textbf{ConstStr}(``.")
\label{p:concatenate-example}
	     \end{program}
	\end{mdframed}

The output string is the concatenation of four smaller strings: the last name, the constant ``, ", the first initial, and the constant ``.".
The last name is the string between the space and the last position in the input string $v$.
The first initial is the first character in the input string $v$.
