% !TEX root =  lipics-sample-article.tex
\section{Approach} 
\label{s:technique}

This section discusses our inductive programming technique for performing repetitive code changes using examples. 
Our key \emph{insight} is that we can reduce a more complicated problem of applying repetitive code changes into two simpler subproblems: 
(1) locating similar code fragments that will be changed, and 
(2) applying the desired transformation to each of these code fragments. 
We then extend state-of-the-art IP systems to solve these subproblems: 
we extend FlashExtract~\cite{LE14FLAS} to locate the code fragments, 
and extend FlashFill~\cite{GU11AUTO} to perform the transformation.

	\begin{figure*}[t]
		\centering
		\includegraphics[scale=0.5]{images/bigpicture.png}
		\caption{The process of performing repetitive code changes using examples.}
		\label{fig:bigpicture}	
	\end{figure*}

Figure~\ref{fig:bigpicture} illustrates the four steps developers use to refactor their code with our approach.
In Step 1, the developer selects code fragments to apply the change as examples.
In Step 2, these examples are used  to synthesize a program to locate all code fragments that will be changed. 
The developer then edits a subset of these code fragments, which are used to learn the programs to 
perform the change (Step 3). 
In Step 4, the top-ranked program is chosen to perform the actual transformation.


%\end{itemize}

%We discuss these steps in details next.

\subsection{Locating source code fragments using examples} 
\label{s:feprogram}

The original FlashExtract would not be able to identify the to-be-changed code fragments in most of our benchmarks.
We make the following changes in FlashExtract to handle the ASTs:
\begin{itemize}
\item We take into account the host language tokens for the learning.
\item We extend the filtering condition to support arbitrary conjunctions.
\item We extend the notion of region from two character positions to the region formed by two AST nodes or a single node (in the latter case, the region includes the node and all of its children).
\end{itemize}

%\begin{figure}
%	\begin{mdframed}[skipabove=2pt,skipbelow=0pt]
%		\begin{grammar}
%		   % a gente nao implementou isso
%		   <Pair Seq $N_{1}$> ::= \textbf{Merge}($SS_{1}$,...,$SS_{n}$)
%		   
%		     %nao sei se a gente precisa falar disso agora, deixar mais simples
%			%<Pair Region $N_{2}$> := \textbf{Pair}(Pos(Stm, $p_{1}$), (Pos(Stm, $p_{1}$))			
%			
%			<Pair Region $N_2$> := let x = $v$ in \textbf{Pair}($p_{1}$, $p_{2}$)
%				
%			<Pair Seq SS> := Node\textbf{Map}($\lambda$x: Pair($p_{1}$, $p_{2}$), NSeq)
%			
%			<Node Seq NSeq> := \textbf{Filter}($\lambda$x: b, FindNodesByType($v$,t)) 
%			
%			<Position expression p> := AbPos(x, k) | RegPos(x, $r_{1}$, $r_{2}$, c)	
%			
%			<Regex r> := T\{0,3\}		
%			
%			<Token T> := C+ | DynamicToken		
%			
%			<Bool b> := $pd_1$ $\wedge$$ \dots$  $\wedge$  $pd_n$	
%			
%			<Predicate pd> := Contains(x, r, c) | NotContains(x, r, c)	
%		\end{grammar}
%	\end{mdframed}
%		\caption{\tool{}'s DSL to locate code fragments that will be changed.}
%		\label{fig:locdsl}
%	\end{figure}

\subsubsection{Domain-Specific Language} 
Figure~\ref{fig:locdsl} shows the DSL to locate source code fragments.
The input to any program is a free variable $v$ of type Region, which is an AST node or a pair of AST nodes.
For example, $v$ can be a class node. In this case, the DSL program will locate code fragments within this class. 

\begin{figure}
	\begin{mdframed}[skipabove=2pt,skipbelow=0pt]
		\begin{grammar}
			% a gente nao implementou isso
			<Pair Seq $N_{1}$> ::= \textbf{Merge}($SS_{1}$,...,$SS_{n}$)
			
			%nao sei se a gente precisa falar disso agora, deixar mais simples
			%<Pair Region $N_{2}$> := \textbf{Pair}(Pos(Stm, $p_{1}$), (Pos(Stm, $p_{1}$))			
			
			<Pair Region $N_2$> := let x = $v$ in \textbf{Pair}($p_{1}$, $p_{2}$)
			
			<Pair Seq SS> := Node\textbf{Map}($\lambda$x: Pair($p_{1}$, $p_{2}$), NSeq)
			
			<Node Seq NSeq> := \textbf{Filter}($\lambda$x: b, FindNodesByType($v$,t)) 
			
			<Position expression p> := AbPos(x, k) | RegPos(x, $r_{1}$, $r_{2}$, c)	
			
			<Regex r> := T\{0,3\}		
			
			<Token T> := C+ | DynamicToken		
			
			<Bool b> := $pd_1$ $\wedge$$ \dots$  $\wedge$  $pd_n$	
			
			<Predicate pd> := Contains(x, r, c) | NotContains(x, r, c)	
		\end{grammar}
	\end{mdframed}
	\caption{The DSL to locate code fragments that will be changed.}
	\label{fig:locdsl}
\end{figure}

%map
The top-level non-terminal $N1$ is a $NodeMap$ operator, which maps each node of a node sequence NSeq to a pair of node positions within that node. 
%filter
The line sequence non-terminal NSeq is a $Filter$ operator. The sequence expression $GetNodesByType(v,t)$ returns nodes of type $t$ within $v$. Predicate $b$ is either $Contains(x, r, c)$, which asserts if node $x$ contains $c$ occurrences of regex $r$, or the negation of it, $NotContains(x, r, c)$.

%pair
The position $p$ is either an absolute position or the position relative to the $k^{th}$ occurrence of a position sequence defined by the regex pair $rr$, which consists of two regexes, $r1$ and $r2$. 
%position
The position sequence ($r1$, $r2$) is the set of all positions $p$ in a lexeme sequence such that the left side of $p$ matches with $r1$ (a
prefix), and the right side of $p$ matches with $r2$ (a suffix). A regex is a concatenation of up to three tokens, and T is the standard
class of tokens of the C\# programming language. We also learn context-sensitive \emph{dynamic tokens}.



To illustrate our DSL, consider the repetitive change shown in Figure~\ref{fig:example1}. Suppose we select the highlighted code as examples of code fragments to apply the repetitive change. Program~\ref{p:flashextract-example1} can be used to locate all similar code fragments in the program.
%explain
In this program, the filter operator returns equal expressions that contain the \code{CSharpKind} method. Then, the map operator applies the pair operator to delimiter the region within each equals expression. In this case, the region is the whole expression.

\begin{mdframed}[skipabove=2pt,skipbelow=0pt]
	\begin{program}\label{p:flashextract-example1}
		\textit{NodeMap($\lambda$x :Pair(p1,p2), NSeq), where \newline
			 p1  = RegPos(x, $\epsilon$, CSharpKind(), 1)), p2 =AbsPos(x, -1) \newline
			%TODO mudar o tipo do metodo
			NSeq = Filter($\lambda$x :Contains(x, CSharpKind()), 
GetNodesByType($v$, EqualsExpressions))}
	\end{program}
\end{mdframed}	


{\tolerance=10000 As a second example, consider the repetitive change shown in Figure~\ref{fig:example2}.  In commit \textit{673f18e}, the developer changed the signature of the method~\code{VerifyRuleSetError}. He altered the type of the second parameter from \code{string} to \code{Func<string>}, and removed the fourth parameter (\code{locMessage}). Then, he performed a repetitive change on every invocation to this method. However, there are two different patterns of changes. If the method invocation do not use the fourth parameter, the developer just replaces the string value passed as the second argument to a lambda function based on this value. On the other hand, in code fragments where the developer passes a variable to the fourth parameter, the developer also deletes the statement that declares this variable and does not pass it to the method invocation.\par}


\begin{figure}[ht]
	\centering
	\includegraphics[scale=0.36]{images/example2.png}
	\caption{Repetitive change applied to Roslyn to update references after changing the method signature.}
	\label{fig:example2}
\end{figure}

Program~\ref{p:flashextract-merge} selects code fragments to perform the repetitive change shown in Figure~\ref{fig:example2}. The program synthesizes two map expressions that are combined by using a merge operator. $SS1$ first selects block statements that contain the invocation to  the method \code{VerifyRuleSetError}. For each of these block statements, $SS1$ selects the region starting with the definition of the \code{locMessage} variable and ending after the invocation to the method \code{VerifyRuleSetError}. $SS2$ selects the expression statements that contain the method \code{VerifyRuleSetError}. 

\begin{mdframed}[nobreak = true, skipabove=2pt,skipbelow=0pt]
\begin{program}\label{p:flashextract-merge}
	\textit{Merge($SS_1$,$SS_2$), where \newline
		\hspace*{2em} SS1 = NodeMap($\lambda$x : Pair(p1, p2), NSeq1) \newline
			\hspace*{4em} p1  = RegPos(x, $\epsilon$, [StringToken, locMessage], 1)) \newline
			\hspace*{4em} p2 =  RegPos(x, [locMessage);], $\epsilon$, 1)) \newline
			\hspace*{4em} NSeq1 = Filter($\lambda$x : Contains(x, VerifyRuleSetError),  \newline 
			\hspace*{6em} GetNodesByType($v$, BlockSyntax)) \newline
		\hspace*{2em} SS2 = NodeMap($\lambda$x : Pair(p1,p2), NSeq2) \newline
			\hspace*{4em} p1  = AbsPos(x, 1), p2 =AbsPos(x, -1) \newline		
			\hspace*{4em} NSeq2 = Filter($\lambda$x : Contains(x, VerifyRuleSetError), \newline
			\hspace*{6em}GetNodesByType($v$, ExpressionStatement))}
\end{program}
	
\end{mdframed}

\subsubsection{Program Synthesis}

The synthesis process to learn programs in the DSL in Figure~\ref{fig:locdsl} is similar to the one described in~\cite{LE14FLAS}. We highlight some important steps:

\begin{itemize}
	\item \textbf{Learning map operator:} We can divide the task of learning a map expression into two smaller tasks: learning pair expressions and filter expressions. The result is the cross-products of the results of the sub-problems;
	
	\item \textbf{Learning merge operator:} We minimize the number of partitions;
	  
	%Given the examples of code fragments to locate to learn the map expression, we can create }} 
	\item \textbf{Learning filter operator:} To learn the sequence expression, for each example, we search for the nearest common ancestor for the selected nodes. To learn the contains predicate, we use brute force search; 
	\item \textbf{Learning pair operator:} We reduce the problem of learning the pair operator to two independent learning problems.
 
\end{itemize} 
	
\subsection{Code transformation using examples} 
\label{s:ffprogram}

We now present our extension to FlashFill to transform the code fragments identified by the previous step using examples.

\subsubsection{Domain-Specific Language}

%\begin{figure}
%	\begin{mdframed}[skipabove=2pt,skipbelow=0pt]
%	\begin{grammar}	
%		<Program Expression p $N_{1}$> ::= \textbf{Switch}($(b_{1}, e_{1})$,...,$(b_{n}, e_{n})$)
%		
%		<Boolean predicate b> ::= \textbf{Contains}(s, r) | \textbf{NotContains}(s, r)
%		
%		<Trace expression e> := \textbf{Concatenate}($f_{1},...,f_{k}$)
%		
%		<Atomic expression f> := \textbf{ConstTokens}(ts) | \textbf{SubTokens}($p_{1}, p_{2}$)
%		
%		<Token sequence ts> := LanguageToken | $\textless$LanguageToken, ts$\textgreater$
%		
%		<Position attribute> := \textbf{AbsPos}(s, k) | \textbf{RegPos}(s, $r_{1}$, $r_{2}$, c)
%		
%		<Regex r> := t | \textbf{TokenSeq}($t_{1},...,t_{n}$)
%		
%		<Token t> := LanguageToken | \textbf{DynToken}( $\textless$<identifier>$\textgreater$.type) | $\epsilon$				
%	\end{grammar}
%   \end{mdframed}
%	\caption{\tool{}'s DSL to transform the code fragments using examples.}
%	\label{fig:editdsl}
%\end{figure}

Figure~\ref{fig:editdsl} show the syntax of our FlashFill extension.
The input to synthesize programs is the set of all highlighted regions identified by the previous step. 
Developers edit a few of these regions, resulting in a set of input-output examples ($s_i$, $o_i$).
The goal of our this step is to synthesize a program that produces $o_i$ given $s_i$.
For instance, in the motivation example, an input-output example is the region between the first $p_1$ and second position $p_2$ edited by the developer.

\begin{figure}
	\begin{mdframed}[skipabove=2pt,skipbelow=0pt]
		\begin{grammar}	
			<Program Expression p $N_{1}$> ::= \textbf{Switch}($(b_{1}, e_{1})$,...,$(b_{n}, e_{n})$)
			
			<Boolean predicate b> ::= \textbf{Contains}(s, r) | \textbf{NotContains}(s, r)
			
			<Trace expression e> := \textbf{Concatenate}($f_{1},...,f_{k}$)
			
			<Atomic expression f> := \textbf{ConstTokens}(ts) | \textbf{SubTokens}($p_{1}, p_{2}$)
			
			<Token sequence ts> := LanguageToken | $\textless$LanguageToken, ts$\textgreater$
			
			<Position attribute> := \textbf{AbsPos}(s, k) | \textbf{RegPos}(s, $r_{1}$, $r_{2}$, c)
			
			<Regex r> := t | \textbf{TokenSeq}($t_{1},...,t_{n}$)
			
			<Token t> := LanguageToken | \textbf{DynToken}( $\textless$<identifier>$\textgreater$.type) | $\epsilon$				
		\end{grammar}
	\end{mdframed}
	\caption{The DSL to transform the code fragments using examples.}
	\label{fig:editdsl}
\end{figure}

%switch
The top level of the DSL is the Switch construct that contains several tuples ($b_i$, $e_i$). 
If the Boolean predicate $b_i$ is satisfied, the trace expression $e_i$ will execute. 
The trace expression concatenates several atomic expressions. 
%SubTokens
The atomic expression SubTokens(p1, p2) forms a sequence of tokens between two positioning expressions $p_i$ and  $p_2$. 
%ConstTokens
The expression ConstTokens (tokens) produces a sequence of constants tokens.
%Other operators
The other constructs have the same semantics of the constructs defined in the FlashExtract framework.

%example
To illustrate our DSL, consider the repetitive change shown in Figure~\ref{fig:example1}. Suppose we edited the highlighted code as examples of transformations to apply the repetitive change. Program~\ref{p:flashfill-example1} can be used to transform all similar code fragments in the program.
%explain
The concatenate expression concatenates three atomic expression. The first atomic expression produces \code{IsKind(}. The second atomic expression produces the set of tokens after the EqualsToken to the end of the input. The last atomic expression produces the closing \code{)}.

\begin{mdframed}[nobreak = true]
	\begin{program}
	\textit{Concatenate(ConstTokens(``IsKind(''), SubTokens(p1,p2), ConstTokens(``)'')), where
p1 = RegPos(s, EqualsToken,$\epsilon$,1), p2 = AbsPos(s, -1)}
	\label{p:flashfill-example1}
	\end{program}
\end{mdframed}

As a second example, consider the repetitive change shown in Figure~\ref{fig:example2}. 
Our technique learns Program~\ref{p:flashfill-example2} for this task. 
The program contains a Switch expression with two branches.
The first branch $(b_1, e_1)$ edits the code fragments with the \code{locMessage} variable, as described by the predicate $b_1$.
The second branch $(b_2, e_2)$ handles the code fragments that do not have the \code{locMessage} variable. 

\begin{mdframed}[nobreak=true]
	\begin{program}
	\textit{Switch((b1, e1), (b2, e2)), where \newline
	\hspace*{1em} b1 = Constains(s, locMessage), b2 = NotContains(s, locMessage) \newline
	\hspace*{1em} e1 = Concatenate(f1, f2, f3, f4),
f1 = SubTokens(p1,p2) \newline
			\hspace*{3em} p1 = RegPos(s, $\epsilon$,VerifyRuleSetError,1),
p2 = RegPos(s, CommaToken,$\epsilon$,1) \newline
		\hspace*{2em} f2 = ConstTokens("()=$>$"), 		 
f3 = SubTokens(p1,p2) \newline
			\hspace*{3em} p1 = RegPos(s, CommaToken,$\epsilon$,1),
p2 = RegPos(s, $\epsilon$,CommaToken,-1) \newline
		\hspace*{2em} f4 = ConstTokens(");") \newline
	\hspace*{1em} e2 = Concatenate(f1, f2, f3),
 f1 = SubTokens(p1, p2) \newline
			\hspace*{3em} p1 = AbsPos(s, 1), p2 = RegPos(s, CommaToken,$\epsilon$,1) \newline
		\hspace*{2em} f2 = ConstTokens("() =$>$"),
f3 = SubTokens(p1, p2)	\newline
			\hspace*{3em} p1 = RegPos(s, CommaToken,$\epsilon$,1), p2 = AbsPos(s, -1)}
	\label{p:flashfill-example2}
	\end{program}
\end{mdframed}	

\subsubsection{Program Synthesis}

We present some key steps of this learning transformation process:

\begin{itemize}
	\item \textbf{Synthesizing programs for each example}: we synthesize all possible programs that satisfy each input/output example; 
	%he synthesis of SubTokens expression is divided into two problems: learning each token position argument.  
	
	\item \textbf{Intersecting the programs learned from each example}: we then join the programs synthesized for each individual examples.
        The result is the set of programs satisfying all examples. If this is not possible, we partition the examples and learn a program
        for each partition. We also minimize the number of partitions;
	
	\item \textbf{Create a Boolean classifier for each branch}: If there is more than one partition, we synthesize a Boolean predicate for each branch.
\end{itemize}
