Log <- read.csv2("~/Modularity/artifacts/Log.csv")

library(ggplot2)
library(scales)
ggp <- ggplot(data.frame(Log$X.exloc),aes(x=Log$X.exloc))
# counts

m <- ggplot(Log, aes(x = locs))

#command to plot the distribution of repetitive changes per commit.
m <- m + geom_histogram(binwidth=1, colour="black", fill="white") +
  geom_vline(aes(xintercept=median(locs, na.rm=T)),   # Ignore NA values for mean
             color="blue", linetype="dashed", size=1) + scale_y_continuous(breaks= pretty_breaks()) + scale_x_continuous(breaks= seq(from = 0, to = 60, by =  5)) + ylab("Occurrences") + xlab("Locations at developer commits")

m + theme(axis.text.x=element_text(colour="black"), axis.text.y = element_text(colour="black"))
#Repetitiva changes features
change_features <- read.csv("~/Modularity/artifacts/change_features.csv", sep=";", na.strings="NA")

apply(change_features,2,max,na.rm=TRUE)# this will remove the NA's from columns that contain them

#count number of changes that occurs in multiple docs.
md <- grep("Multiple docs.", change_features[, 2])

#count number of changes that occurs in single doc.
sd <- grep("Single doc.", change_features[, 2])

length(md) / (length(md) + length(sd))

#count number of changes that are similar
sc <- grep("No", change_features[, 3]);

#count number of changes that are identical
ic <- grep("Yes", change_features[, 3]);

length(sc) / (length(sc) + length(ic));


#count number of changes that occurs in multiple lines.
ml <- grep("Multiple lines", change_features[, 4]);

#count number of changes that occurs in single line.
sl <- grep("Single line", change_features[, 4]);

length(ml) / (length(ml) + length(sl))


#Tool evaluation
#Location
#mean examples to locate
ex.loc = Log[, "X.negs....exloc"]
mean(ex.loc)

#number of cases in which the locations found by the tool are equals to the location on the commit.
eq.commit <- Log[Log$ac.loc == Log$locs, ]

#proportion
nrow(eq.commit) / nrow(Log)

#number of cases in which the locations found by the tool are more than the number of locations on the commit.
more.commit <- Log[Log$ac.loc > Log$locs, ]
nrow(more.commit)


#Transformation
#mean examples to locate
ex.trans = Log[, "X.extrans"]
mean(ex.trans)


#time to learn location program
time.lc.learn = Log[, "time.lc.learn..s."]
mean(time.lc.learn)

#time to run the location program
time.lc.run = Log[, "time.lc.run..s."]
mean(time.lc.run)


#time to learn a program to apply the repetitive change
time.trans.learn = Log[, "time.trans.learn..s."]
mean(time.trans.learn)

#time to learn a program to apply the repetitive change
time.trans.run = Log[, "time.trans.run..s."]
mean(time.trans.run)


#code to plot the images of the commits that has more locations that those present on the commit.
format.more <- function(list){
  fr <- vector()
  frv <- vector()
  frf <- vector()
  for (i in 1:nrow(list) ){
   ncm <- list[i,]$locs
   nto <- list[i,]$ac.loc
   print(ncm)
   print(nto)
   rp <- rep(list[i,]$commit, 2)
   #ncml <- rep("developer", ncm)
   #ntol <- rep("tool", nto)
   rpv <- as.vector(rp)
   
   fr <- c(fr, rpv)
   
   frv <- c(frv, "developer")
   frv <- c(frv, "tool")
   
   frf <- c(frf, c(ncm, nto))
   
   #print(rpv)
  }
  ret <- data.frame(fr, frv, frf)
  names(ret) <- c("commit", "Owner", "Freq")
  ret
}

format.m <- format.more(more.commit)

a <- ggplot(format.m, aes(factor(commit), Freq, fill = Owner)) + 
  geom_bar(position="dodge",stat="identity", width = 0.5)
a = a + xlab("Commits") + ylab("Occurrences") 

p5 <- a + theme(axis.text.x = element_text(angle = 90, hjust = 1, colour = "black"), axis.text.y = element_text(colour = "black")) + scale_y_continuous(breaks= seq(from = 0, to = 900, by =  250))
p5
#format.m <- format.more(more.commit)

#a <- plot(format.m, aes(factor(commit), Freq, fill = Owner))  
#a <- a + geom_bar(position="dodge", stat="identity", width = 0.7)
#a = a + xlab("Commits") + ylab("Occurrences") 

#a + theme(axis.text.x = element_text(angle = 90, hjust = 1, colour = "black"), axis.text.y = element_text(colour = "black"))

#theme(axis.text.x=element_text(colour="black"), axis.text.y = element_text(colour="black"))

#the examples to each repetitive change (location)
format.examples <- function(list){
  fr <- vector()
  frv <- vector()
  frf <- vector()
  for (i in 1:nrow(list) ){
    ncm <- list[i,]$X.exloc
    nto <- list[i,]$X.negs
    print(ncm)
    print(nto)
    
    #rp <- rep(list[i,]$commit, 2)
    rp <- rep(list[i,]$commit, 1)
    #ncml <- rep("developer", ncm)
    #ntol <- rep("tool", nto)
    rpv <- as.vector(rp)
    
    fr <- c(fr, rpv)
    
    frv <- c(frv, "loc.examples")
    #frv <- c(frv, "loc.negatives")
    
    frf <- c(frf, c(ncm + nto))
    
    #print(rpv)
  }
  ret <- data.frame(fr, frv, frf)
  names(ret) <- c("commit", "Type", "Freq")
  ret
}

format.ex.loc <- format.examples(Log)

a <- ggplot(format.ex.loc, aes(factor(commit), Freq, fill = Type)) + 
  geom_bar(stat="identity", width = 0.5)
a = a + xlab("Commits") + ylab("Occurrences") 

p1 <- a + theme(axis.text.x = element_text(angle = 90, hjust = 1, colour = "black"), axis.text.y = element_text(colour = "black"))
p1

#the examples to each repetitive change (transform)
format.examples <- function(list){
  fr <- vector()
  frv <- vector()
  frf <- vector()
  for (i in 1:nrow(list) ){
    ncm <- list[i,]$X.extrans
#    nto <- list[i,]$X.negs
    print(ncm)
#    print(nto)
    
    #rp <- rep(list[i,]$commit, 2)
    rp <- rep(list[i,]$commit, 1)
    #ncml <- rep("developer", ncm)
    #ntol <- rep("tool", nto)
    rpv <- as.vector(rp)
    
    fr <- c(fr, rpv)
    
    frv <- c(frv, "trans.examples")
    #frv <- c(frv, "trans.negatives")
    
    #frf <- c(frf, c(ncm, 0))
    frf <- c(frf, c(ncm))
    
    #print(rpv)
  }
  ret <- data.frame(fr, frv, frf)
  names(ret) <- c("commit", "Type", "Freq")
  ret
}

format.ex.trans <- format.examples(Log)

a <- ggplot(format.ex.trans, aes(factor(commit), Freq, fill = Type)) + 
  geom_bar(stat="identity", width = 0.5)
a = a + xlab("Commits") + ylab("Occurrences") 

p2 <- a + theme(axis.text.x = element_text(angle = 90, hjust = 1, colour = "black"), axis.text.y = element_text(colour = "black")) + scale_y_continuous(breaks= seq(from = 0, to = 8, by =  2))
p2

format.ex <- rbind(format.ex.loc, format.ex.trans)

a <- ggplot(format.ex, aes(factor(commit), Freq, fill = Type)) + 
  geom_bar(position="dodge", stat="identity", width = 0.5)
a = a + xlab("Commits") + ylab("Occurrences") 

p3 <- a + theme(axis.text.x = element_text(angle = 90, hjust = 1, colour = "black"), axis.text.y = element_text(colour = "black")) + scale_y_continuous(breaks= seq(from = 0, to = 8, by =  2))
#p3 <- p3 + scale_fill_brewer(palette="Greys")
p3


m <- ggplot(format.ex.loc, aes(x = Freq))
m <- m + geom_histogram(binwidth=0.3, colour="black", fill="white") + scale_y_continuous(breaks= pretty_breaks()) + scale_x_continuous(breaks= seq(from = 0, to = 10, by =  1)) + ylab("Occurrences") + xlab("Example required to locate")
h1 <- m + theme(axis.text.x = element_text(colour = "black")) + theme(axis.text.y = element_text(colour = "black"))

m <- ggplot(format.ex.trans, aes(x = Freq))
m <- m + geom_histogram(binwidth=0.3, colour="black", fill="white") + scale_y_continuous(breaks= pretty_breaks()) + scale_x_continuous(breaks= seq(from = 0, to = 10, by =  1)) + ylab("Occurrences") + xlab("Examples required to edit")
h2 <- m + theme(axis.text.x = element_text(colour = "black")) + theme(axis.text.y = element_text(colour = "black"))


multiplot(h1, h2, cols =2)

# Multiple plot function
#
# ggplot objects can be passed in ..., or to plotlist (as a list of ggplot objects)
# - cols:   Number of columns in layout
# - layout: A matrix specifying the layout. If present, 'cols' is ignored.
#
# If the layout is something like matrix(c(1,2,3,3), nrow=2, byrow=TRUE),
# then plot 1 will go in the upper left, 2 will go in the upper right, and
# 3 will go all the way across the bottom.
#
multiplot <- function(..., plotlist=NULL, file, cols=1, layout=NULL) {
  library(grid)
  
  # Make a list from the ... arguments and plotlist
  plots <- c(list(...), plotlist)
  
  numPlots = length(plots)
  
  # If layout is NULL, then use 'cols' to determine layout
  if (is.null(layout)) {
    # Make the panel
    # ncol: Number of columns of plots
    # nrow: Number of rows needed, calculated from # of cols
    layout <- matrix(seq(1, cols * ceiling(numPlots/cols)),
                     ncol = cols, nrow = ceiling(numPlots/cols))
  }
  
  if (numPlots==1) {
    print(plots[[1]])
    
  } else {
    # Set up the page
    grid.newpage()
    pushViewport(viewport(layout = grid.layout(nrow(layout), ncol(layout))))
    
    # Make each plot, in the correct location
    for (i in 1:numPlots) {
      # Get the i,j matrix positions of the regions that contain this subplot
      matchidx <- as.data.frame(which(layout == i, arr.ind = TRUE))
      
      print(plots[[i]], vp = viewport(layout.pos.row = matchidx$row,
                                      layout.pos.col = matchidx$col))
    }
  }
}